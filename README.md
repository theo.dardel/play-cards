### In a nutshell
Play with cards in a web browser: display, flip, remove, reorder or make a copy 
in the sandbox to rotate or resize

Made with html, javascript ([konva.js canvas library](https://konvajs.org/)), css

Made for Firefox browser

Example cards by David Bellot [http://svg-cards.sourceforge.net/](http://svg-cards.sourceforge.net/)

### How to play
Open [play-cards.html]() with (Firefox) web browser to start playing.
![12 cards on their back](how-to/open_html_file_see_cards_back.png?raw=true "Open play-cards.html file in browser")

Click on a card to flip it, click another time to hide it, and click one more time to show it again.
![12 cards on their front, ordered by suits](how-to/click_on_back_see_front.png?raw=true "Click to flip a card")

Drag and drop to reorder.
![8 cards on their front - 4 Jacks, 4 Queens, 1 card on its back, 3 hidden cards](how-to/click_on_front_to_hide_drag_drop_to_reorder.png?raw=true "Click again to hide. Drag and drop to reorder.")

Drag and drop a card on the right bar to send a copy to the sandbox.
![Right bar lightens when you drag a card above it](how-to/drag_drop_on_right_bar_to_copy_to_sandbox.png?raw=true "Drag and drop on the right bar to copy a card to the sandbox")

Click on the right bar to make the sandbox appear, and click again to hide it. 
![Sandbox over baize, one big card, one card rotated](how-to/display_sandbox_rotate_resize.png?raw=true "Click on right bar, sandbox appears - click again, sandbox disappears")
Once a card is in the sandbox, you can resize or rotate it. Also, click to send to the foreground, double-click to erase. 


Well, playing with the example cards is possibly not really fun. So, feel free to replace them!